import socket 
import encodings
import time
from datetime import datetime


HOST = '192.168.0.13'  
PORT = 8080        

def my_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print("Server Started: Waiting for connection [",datetime.now(), "]")
        s.bind((HOST, PORT))
        s.listen(5)
        conn, addr = s.accept()

        with conn:
            print('New Connection:', addr , "[",datetime.now(), "]")
            while True:
                client_data = conn.recv(1024).decode('utf-8')

if __name__ == '__main__':
    while 1:
        my_server()
