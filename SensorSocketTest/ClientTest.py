try:
    import socket
    import threading
    import time
    from datetime import datetime
except:
    print('library not found ')


HOST = '192.168.0.13'  # The server's hostname or IP address
PORT = 8080       # The port used by the server


def process_data_from_server(x):        # Define function to split Incoming Data
    x1, y1 = x.split(",")
    return x1, y1


def my_client():

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:        # define socket TCP
        s.connect((HOST, PORT))

        my_inp = 'Connection'

        # encode the message
        my_inp = my_inp.encode('utf-8')

        # send request to server
        s.send(my_inp)
    
        while True:
            data_req = "Data_Request"
            
            data_req = data_req.encode('utf-8')
            
            s.send(data_req)
            
            # Get the Data from Server and process the Data
            data = s.recv(1024).decode('utf-8')



            # Process the data - split comma seperated value
            x_temperature,y_humidity = process_data_from_server(data)
            print("Data received [",datetime.now(), "]")
            
            print("Temperature {}".format(x_temperature))
            print("Humidity {}".format(y_humidity))
            time.sleep(30)
        
        
   

if __name__ == "__main__":
    while 1:
        my_client()
