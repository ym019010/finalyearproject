import socket 
import numpy as np
import encodings
import time
import Adafruit_DHT
import jpysocket
from datetime import datetime


HOST = '192.168.0.13'  # Standard loopback interface address (localhost)
PORT = 8080        # Port to listen on (non-privileged ports are > 1023)


def sensor_data():
    pin = 4
    sensor = Adafruit_DHT.DHT22
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

    
    while True:
        print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
        print(" Data Sent: T{0:0.1f} H{1:0.1f}".format(temperature,humidity))
        data = '{},{}'.format(temperature,humidity)
        time.sleep(10)
        return data


def my_server():
    try:
        time.sleep(3)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            print("Server Started: Waiting for connection [",datetime.now(), "]")
           
            s.bind((HOST, PORT))
            s.listen(5)
            conn, addr = s.accept()

            with conn:
                print('New Connection:', addr , "[",datetime.now(), "]")
                
                while True:

                    client_data = conn.recv(1024).decode('utf-8')

                    if str(client_data) == "Connection":

                        print("Connection Established: Packets Received [",datetime.now(), "]")
                        
                        
                    elif str(client_data) == "Data_Request":
                        print("Data requested [",datetime.now(), "]")
                       
                        data_to_send = sensor_data()

                        x_encoded_data = jpysocket.jpyencode(data_to_send + "\n")

                        conn.sendall(x_encoded_data)
                        
                        
                    else:
                        print("Unknown Request : '", str(client_data), "' [",datetime.now(), "]")
                        break
                    
    except Exception as e:
        my_server()
                   


if __name__ == '__main__':
    while 1:
        my_server()
