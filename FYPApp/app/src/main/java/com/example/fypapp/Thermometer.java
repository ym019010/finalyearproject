package com.example.fypapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;


public class Thermometer extends View {

    private float outerCircRad, outRectRad;
    private Paint outPnt;
    private float midCircRad, midRectRad;
    private Paint midPnt;
    private float innerCircRad, innerRectRadius;
    private Paint inPnt;
    private Paint dgrPnt, lblPnt;
    private static float DEGREE_WIDTH = 30;
    private static final int NB_GRADUATIONS = 8;
    public static final float MAX_TEMP = 70, MIN_TEMP = -10;
    private static final float RANGE_TEMP = 80;
    private int nbLbls = NB_GRADUATIONS;
    private float maxTemp = MAX_TEMP;
    private float minTemp = MIN_TEMP;
    private float rangeTemp = RANGE_TEMP;
    private float currentTemp = MIN_TEMP;
    private Rect rect = new Rect();

    public Thermometer(Context context) {
        super(context);
        init(context, null);
    }

    public Thermometer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public Thermometer(Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setCurrentTemp(float currentTemp) {
        if (currentTemp > maxTemp) {
            this.currentTemp = maxTemp;
        } else if (currentTemp < minTemp) {
            this.currentTemp = minTemp;
        } else {
            this.currentTemp = currentTemp;
        }

        invalidate();
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Thermometer);
        outerCircRad = typedArray.getDimension(R.styleable.Thermometer_radius, 20f);
        int outerCol = typedArray.getColor(R.styleable.Thermometer_outerCol, Color.GRAY);
        int midCol = typedArray.getColor(R.styleable.Thermometer_midCol, Color.WHITE);
        int innerCol = typedArray.getColor(R.styleable.Thermometer_innerCol, Color.RED);

        typedArray.recycle();

        outRectRad = outerCircRad / 2;
        outPnt = new Paint();

        outPnt.setColor(outerCol);
        outPnt.setStyle(Paint.Style.FILL);

        midCircRad = outerCircRad - 5;
        midRectRad = outRectRad - 5;
        midPnt = new Paint();
        midPnt.setColor(midCol);
        midPnt.setStyle(Paint.Style.FILL);

        innerCircRad = midCircRad - midCircRad / 6;
        innerRectRadius = midRectRad - midRectRad / 6;
        inPnt = new Paint();
        inPnt.setColor(innerCol);
        inPnt.setStyle(Paint.Style.FILL);

        DEGREE_WIDTH = midCircRad / 8;

        dgrPnt = new Paint();
        dgrPnt.setStrokeWidth(midCircRad / 16);
        dgrPnt.setColor(outerCol);
        dgrPnt.setStyle(Paint.Style.FILL);

        lblPnt = new Paint();
        lblPnt.setColor(outerCol);
        lblPnt.setStyle(Paint.Style.FILL);
        lblPnt.setAntiAlias(true);
        lblPnt.setTextSize(75);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int height = getHeight();
        int width = getWidth();

        int circleCenterX = width / 2;
        float circleCenterY = height - outerCircRad;
        float outerStartY = 0;
        float middleStartY = outerStartY + 5;

        float innerEffectStartY = middleStartY + midRectRad + 10;
        float innerEffectEndY = circleCenterY - outerCircRad - 10;
        float innerRectHeight = innerEffectEndY - innerEffectStartY;
        float innerStartY = innerEffectStartY + (maxTemp - currentTemp) / rangeTemp * innerRectHeight;

        RectF outerRect = new RectF();
        outerRect.left = circleCenterX - outRectRad;
        outerRect.top = outerStartY;
        outerRect.right = circleCenterX + outRectRad;
        outerRect.bottom = circleCenterY;

        canvas.drawRoundRect(outerRect, outRectRad, outRectRad, outPnt);
        canvas.drawCircle(circleCenterX, circleCenterY, outerCircRad, outPnt);

        RectF midRect = new RectF();
        midRect.left = circleCenterX - midRectRad;
        midRect.top = middleStartY;
        midRect.right = circleCenterX + midRectRad;
        midRect.bottom = circleCenterY;

        canvas.drawRoundRect(midRect, midRectRad, midRectRad, midPnt);
        canvas.drawCircle(circleCenterX, circleCenterY, midCircRad, midPnt);

        canvas.drawRect(circleCenterX - innerRectRadius, innerStartY, circleCenterX + innerRectRadius, circleCenterY, inPnt);
        canvas.drawCircle(circleCenterX, circleCenterY, innerCircRad, inPnt);

        float tmp = innerEffectStartY;
        float startLbl = maxTemp;
        float inc = rangeTemp / nbLbls;

        while (tmp <= innerEffectEndY) {
            canvas.drawLine(circleCenterX - outRectRad - DEGREE_WIDTH, tmp, circleCenterX - outRectRad, tmp, dgrPnt);
            String txt = ((int) startLbl) + "°";
            lblPnt.getTextBounds(txt, 0, txt.length(), rect);
            float textWidth = rect.width();
            float textHeight = rect.height();

            canvas.drawText(((int) startLbl) + "°", circleCenterX - outRectRad - DEGREE_WIDTH - textWidth - DEGREE_WIDTH * 1.5f,
                    tmp + textHeight / 2, lblPnt);
            tmp += (innerEffectEndY - innerEffectStartY) / nbLbls;
            startLbl -= inc;
        }
    }
}