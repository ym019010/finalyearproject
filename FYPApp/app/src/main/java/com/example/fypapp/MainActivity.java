package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("SetTextI18n")
public class MainActivity extends AppCompatActivity {
    Thread Thread1 = null;
    TextView tempReading;
    TextView humReading;
    String SERVER_IP;
    int SERVER_PORT;
    ProgressBar humidBar;
    private Thermometer thermometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SERVER_IP = "192.168.0.13";
        SERVER_PORT = 8080 ;
        try{
        Thread1 = new Thread(new Thread1());
        Thread1.start();}
        catch (Exception e) {

        }
        thermometer = (Thermometer) findViewById(R.id.thermometer);
        tempReading = findViewById(R.id.tempReading);
        humidBar = (ProgressBar) findViewById(R.id.progress_bar);
        humReading = findViewById(R.id.humReading);

    }

    private PrintWriter output;
    private BufferedReader input;
    class Thread1 implements Runnable {
        public void run() {
            Socket socket;
            //attempts to connect
            try {
                socket = new Socket(SERVER_IP, SERVER_PORT);
                //output writer
                output = new PrintWriter(socket.getOutputStream());
                //input reader
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                runOnUiThread(new Runnable() {
                    @Override
                    //upon connection
                    public void run() {

                    }
                });
                //starts the timer thread containing the send and receive functions

                Timer timer = new Timer();
                long delay = 1000;
                long intervalPeriod = 30000 ;
                timer.scheduleAtFixedRate(reqData, delay, intervalPeriod);


            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }
    class Thread2 implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {

                    String message = input.readLine();
                    message = message.replaceFirst("^..", "");
                    float floatConvT = Float.parseFloat(message.split(",")[0]);
                    final String TempInString = String.format("%.01f", floatConvT);
                    int intConvH = Integer.parseInt(message.split(",")[1].substring(0,message.indexOf(".")));
                    final String HumInString = Integer.toString(intConvH);
                    message = addChar(message, 'T',0);
                    message = message.replace(",","\nH");
                    final String messageCleaned = message;

                    if (message != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                thermometer.setCurrentTemp(floatConvT);
                                tempReading.setText("");
                                tempReading.append(TempInString + "°C");
                                humidBar.setProgress(intConvH);
                                humReading.setText("");
                                humReading.append(HumInString + "%");
                            }
                        });
                    } else {
                        Thread1 = new Thread(new Thread1());
                        Thread1.start();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public String addChar(String str, char ch, int position) {
            return str.substring(0, position) + ch + str.substring(position);
        }
    }
    TimerTask reqData = new TimerTask() {
        @Override
        public void run() {
            String request = "Data_Request";
            output.write(request);
            output.flush();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Thread(new Thread2()).start();
                }
            });
        }
    };


}

