import adafruit_dht
import time
import board

dhtSensor = adafruit_dht.DHT22(board.D4)

while True:
        try:
                humidity = dhtSensor.humidity
                temp_c = dhtSensor.temperature
                print(humidity)
                print(temp_c)
                time.sleep(5)
        except RuntimeError:
                print("RuntimeError, trying again...")
                continue
                
